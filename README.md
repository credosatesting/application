# README #

### What is this repository for? ###

* Hosting and setup of Testing Application
* Version 1.0.0

### How do I get set up? ###

Docker Compose:

1. Install Docker 
2. Install Docker Compose
3. Clone repository locally
4. Browse to repository and issue the following command

```
#!bash

docker-compose up -d
```

Docker:

1. Install Docker
2. Execute following commands in order


```
#!bash

docker run -ti -d -p 5000:5000 --name=json-server jwvolschenk/json-server

```
For Linux Distributions:


```
#!bash

docker run -ti -d --link json-server -p 80:80 -e "DH=127.0.0.1" --name=front-end jwvolschenk/front-end
```

For Windows Distributions:


```
#!bash

docker run -ti -d --link json-server -p 8080:80 -e "DH=${DOCKER_HOST}" --name=front-end jwvolschenk/front-end
```

### Who do I talk to? ###

* J.W. Volschenk <jwvolschenk@gmail.com>
* Jethro Sloan <jdksloan@gmail.com>